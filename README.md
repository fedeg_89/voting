# Voting
Node.js Socket no-chat app. No Dependencies.

## Description
This app is the place where the most important decisions take place. Create a new voting room with the topics, options, and its own configuration; find it in the main menu; enter the rooms with your mates via TCP and let the voting session start.

## How to run
Just run node index.js, and connect via netcat running nc localhost 8000. That's all!