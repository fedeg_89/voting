const { getRooms } = require('../classes/Rooms')

const getReadyString = (roomId) => {
	const room = getRooms(roomId)
	let text = `${room.ready}/${room.min} participants ready.\n`
	return text
}

module.exports = getReadyString