module.exports = (character) => {
	return String.fromCharCode(character.charCodeAt(0) + 1);
}
