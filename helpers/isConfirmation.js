module.exports = (text) => {
	if (/^yes$|^y$/i.test(text)) return 'y'
	if (/^no$|^n$/i.test(text)) return 'n'

	return false
}