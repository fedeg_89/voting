const { getRooms } = require('../classes/Rooms')
const getReadyString = require('./getReadyString')

const roomConfigString = (roomId) => {
	const room = getRooms(roomId)
	
	let text = `Room ${roomId} - ${room.name}
${room.desc}
Participants: ${room.current}/${room.max} - MIN: ${room.min}
In the room: `

	room.participants.forEach(participant => {
		text += `${participant}\t`
	})

	text += getReadyString(roomId)
	return text
}

module.exports = roomConfigString