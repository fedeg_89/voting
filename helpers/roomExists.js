const { getRooms } = require('../classes/Rooms')

const roomExists = async (name) => {
	const rooms = await getRooms()
	for (const room in rooms) {
		if (rooms[room].name == name)	return true
	}
}

module.exports = roomExists