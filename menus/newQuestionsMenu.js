const fs = require('fs')
const { promisify } = require('util')
const appendFile = promisify(fs.appendFile)
const unlink = promisify(fs.unlink)
const nextChar = require('../helpers/nextChar')
const isEnd = require('../helpers/isEnd')
const isCancel = require('../helpers/isCancel')
const mainMenu = require('./mainMenu')

const newQuestionsWelcome = "\nLet's do the questions. \nSend end any moment to finish the file; \nsend cancel to exit without saving.\n"
const separator1 = '----------------------------------------\n'
const newQuestion = (questionNumb) =>	`-Enter the question N.${questionNumb}:\n(press enter to finish)\n`
const newOption = (optionlett) =>	`--Enter the option ${optionlett}:\n(pres enter if it's yes/no question or has no more options)\n`
const noOptions = 'a. Yes\tb. No'

async function newQuestionsMenu (socket, data) {
	// First time
	if (socket.newRoom.state == 'room') {
		console.log('First time')
		// Write the title
		let text = 'TOPICS\r\n'
		await appendFile(`./topics/${socket.newRoom.name}.txt`, text)
		// Setting
		socket.newRoom.state = 'questions'
		socket.newRoom.question = 1
		socket.newRoom.option = 'a'
		// Ask to socket
		socket.write(newQuestionsWelcome)
		socket.write(separator1)
		socket.write(newQuestion(socket.newRoom.question))
		return
	}

	if (isEnd(data)) {
		
		socket.write(`\nVoting room ${socket.newRoom.name} created!\n`)
		exitRoom(socket)
		return
	}

	if (isCancel(data)) {
		await unlink(`./topics/${socket.newRoom.name}.txt`)
		socket.write(`\nVoting room ${socket.newRoom.name} canceled\n`)
		exitRoom(socket)
		return
	}
	
	if (socket.newRoom.state == 'questions') { 
		if (!data) {
			socket.write(`\nVoting room ${socket.newRoom.name} created!\n`)
			exitRoom(socket)
			return
		}
		let text = `${socket.newRoom.question}. ${data}\r\n`
		await appendFile(`./topics/${socket.newRoom.name}.txt`, text)
		// Settings for the options
		socket.newRoom.state = 'options'
		socket.newRoom.question ++
		// Ask to socket
		socket.write(newOption(socket.newRoom.option))
		return
	}

	if (socket.newRoom.state == 'options') {
		if (!data) {
			let text = ''
			if (socket.newRoom.option == 'a') text = noOptions // yes/no question
			text += '\r\n'

			await appendFile(`./topics/${socket.newRoom.name}.txt`, text)
			// Settings for the next question
			socket.newRoom.state = 'questions'
			socket.newRoom.option = 'a'
			// Ask to socket
			socket.write(newQuestion(socket.newRoom.question))
			return
		} else {
			let text = `${socket.newRoom.option}. ${data}\t`
			await appendFile(`./topics/${socket.newRoom.name}.txt`, text)
			// Setting for next option
			socket.newRoom.option = nextChar(socket.newRoom.option)
			// Ask to socket
			socket.write(newOption(socket.newRoom.option))
			return
		}

	}

}
function exitRoom(socket) {
	delete socket.newRoom
	delete socket.room
	mainMenu(socket)
}
module.exports = newQuestionsMenu