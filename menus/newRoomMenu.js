const fs = require('fs')

const isConfirmation = require('../helpers/isConfirmation')
const areNumbers = require('../helpers/areNumbers')
const roomExists = require('../helpers/roomExists')

const newQuestionsMenu = require('./newQuestionsMenu')
const mainMenu = require('./mainMenu')

const newRoomWelcome = '\n+++++ Creating new votation room +++++ \n'
const menu = [
	'Name of the votation: ',
	'Description: ',
	'Minimum number of participants: ',
	'Maximum number of participants: ',
	'Quorum (send ? for more info): ',
	'Confirm room settings? y/n\n'
]

const quorumHelp = 	`	*Quorum is the minimum amount of votations to consider it valid;
	*if the votes are less than the Quorum, the topic is considered as "not decided".
	*Values can be a fixed number (5) or a percentage (50%).\n`

let roomsInCreation = {}

const newRoomMenu = async (socket, data) => {
	// First time
	if (socket.newRoom.state != 'room') {
		socket.newRoom.state = 'room'
		socket.write(newRoomWelcome)
		socket.write(menu[0])
		return
	}

	// Room name
	if (socket.newRoom.step == 0) {
		if (roomsInCreation[data]) return socket.write('The room is being created by someone else.\nEnter other name:\n')
		if (await roomExists(data)) return socket.write('The room already exists.\nEnter other name: ')

		roomsInCreation[data] = []
		socket.newRoom.name = data
	}

	// Numbers inputs control
	if (socket.newRoom.step == 2 || socket.newRoom.step == 3) {
		if (!areNumbers(data)) return socket.write('Invalid value. Must be numbers\n')
		if (socket.newRoom.step == 3 && data < roomsInCreation[socket.newRoom.name][2])
		return socket.write('Maximum participants mast be equal or grater than Minimun participants\n')
	}

	// Quorum control
	if (socket.newRoom.step == 4) {
		if (data == '?') return socket.write(quorumHelp)
		const invalidMessage = invalidQuorum(data, socket)
		if (invalidMessage) return socket.write(invalidMessage)
	}

	// Final step control
	if (socket.newRoom.step == menu.length - 1) {
		const confirm = isConfirmation(data)
		if (!confirm) return socket.write('Confirm with Yes or No: ')

		if (confirm == 'y') {
			createNewRoom(roomsInCreation[socket.newRoom.name]).then(()=> {
				delete roomsInCreation[socket.newRoom.name]
				newQuestionsMenu(socket)
			})
			return
		} else {
			delete roomsInCreation[socket.newRoom.name]
			delete socket.newRoom
			delete socket.room
			mainMenu(socket)
			return
		}

	}
	roomsInCreation[socket.newRoom.name][socket.newRoom.step] = data
	socket.newRoom.step ++
	socket.write(menu[socket.newRoom.step])

}

function invalidQuorum(data, socket) {
	// If input is percentage
	if (data[data.length - 1] == '%') {
		qPerc = data.split('%')[0]
		if (!areNumbers(qPerc)) return 'Invalid input.\n'
		qPerc = parseInt(qPerc)
		if (!(qPerc >= 0 && qPerc <= 100)) return 'Must be 0%-100%.\n'
		return false
	}

	// If input is number
	if (areNumbers(data)) {
		let min = parseInt(roomsInCreation[socket.newRoom.name][2])
		let max = parseInt(roomsInCreation[socket.newRoom.name][3])
		data = parseInt(data)
		if (!(data >= min && data <= max)) return 'Must be between min and max participants.\n'
		return false
	} 

	return 'Invalid input.\n'
	
}

function createNewRoom(newRoomSettings) {
	let newRoomStream = fs.createWriteStream(`./topics/${newRoomSettings[0]}.txt`)
	newRoomStream.write('CONFIG\r\n')
	for (let i = 0; i < menu.length -1; i++) {
		newRoomStream.write(`${menu[i]}${newRoomSettings[i]}\r\n`)
	}
	
	return new Promise((resolve, reject) => {
		newRoomStream.end()
		console.log('closed')
		newRoomStream.on('close',() => resolve())
	})
}

module.exports = newRoomMenu