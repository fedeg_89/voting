const newRoomMenu = require('./newRoomMenu')
const newQuestionsMenu = require('./newQuestionsMenu')

const newRoomRouter = (socket, data) => {
	if(!socket.newRoom) socket.newRoom = { step: 0 }
	switch (socket.newRoom.state) {
		case undefined: // First enter
			newRoomMenu(socket)
			break
		case 'room':
			newRoomMenu(socket, data)
			break
		case 'questions':
		case 'options':
			newQuestionsMenu(socket, data)
			break;
		default:
			break
	}
}

module.exports = newRoomRouter
