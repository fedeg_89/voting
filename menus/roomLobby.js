const { isInRoom, joinParticipant, removeParticipant, setReady } = require('../classes/Rooms')
const { sendEverybody } = require('../classes/Messenger')

// const isExit = require('../helpers/isExit')
const getReadyString = require('../helpers/getReadyString')
const roomConfigString = require('../helpers/roomConfigString')

const mainMenu = require('./mainMenu')

const commands = 'Enter /R when you are ready to start. Enter /L to leave the lobby\n'

let text = ''
let roomReady = false

const roomLobby = async (socket, data) => {

	// First enter
	if (!isInRoom(socket.room, socket.name)) {
		// Join the room
		console.log(`Entering room ${socket.room}...`)
		joinParticipant(socket.room, socket.name)
		// Sends everybody that socket entered the room
		text = `${socket.name} has entered the room.\n`
		sendEverybody(text, socket.room, socket)
		// Sends to socket that he entered and display state and participants
		socket.write(`\n * You entered in room ${socket.room} *\n`)

		text = roomConfigString(socket.room)
		sendEverybody(text, socket.room)
		socket.write(getReadyString(socket.room))

		socket.votingRoom = { state: 'notReady' }
		// send the comands for confirm and leave
		socket.write(commands)
		return
	}

	if (data[0] == '/') {
		switch (data) {
			case '/r':
			case '/R':
				if (socket.votingRoom.state == 'notReady') {
					roomReady = setReady(socket.room, true)
					socket.votingRoom.state = 'ready'
					socket.write('You are ready now\n')
				} else if (socket.votingRoom.state == 'ready') {
					setReady(socket.room, false)
					socket.votingRoom.state = 'notReady'
					socket.write('You are not ready.\n')
				}
				sendEverybody(getReadyString(socket.room), socket.room)
				if (roomReady) sendEverybody('Sorry, still working on it :)', socket.room)

				break

			case '/l':
			case '/L':
				removeParticipant(socket.room, socket.name)
				text = `${socket.name} has left the room.\n`
				sendEverybody(text, socket.room)
				text = roomConfigString(socket.room)
				sendEverybody(text, socket.room)
				delete socket.votingRoom
				delete socket.room
				mainMenu(socket)
				break
			default:
				socket.write('Invalid command.\n')
				break
		}
		return
	}

	// Chat
	text = `${socket.name}: ${data}\n`
	sendEverybody(text, socket.room, socket)
	text = `YOU: ${data}\n`
	socket.write(text)

}


module.exports = roomLobby