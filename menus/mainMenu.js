const { getRooms } = require('../classes/Rooms')

let menuTemplate = `
* * * * * M A I N   M E N U * * * * *
0- Create new votation room
`
async function mainMenu(socket) { 
	const rooms = await getRooms()
	if (!rooms) return socket.write('Rooms are not ready yet\n')
	let menu = menuTemplate
	// Fill the menu with the rooms
	for (let room in rooms) {
		const actualRoom = rooms[room]
		if (actualRoom.open) {
			menu += `${room}- Enter room ${actualRoom.name} ${actualRoom.current}/${actualRoom.max} min: ${actualRoom.min}\n`
		}
	}
	socket.write(menu)
}

module.exports = mainMenu
