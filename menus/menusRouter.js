const mainMenu = require('./mainMenu')
const newRoomRouter = require('./newRoomRouter')
const roomLobby = require('./roomLobby')

const areNumbers = require('../helpers/areNumbers')
const roomExists = require('../helpers/roomExists')

const menusRouter = (socket, data) => {
	if (!socket.room && data) { // Cames from mainMenu
		if (!areNumbers(data)) return socket.write('Invalid comand\n')
		if (!roomExists(data) && data != 0) return socket.write('Invalid room\n')
		//
		socket.room = data 
	}
	switch (socket.room) {
		case undefined:
			mainMenu(socket)
			break
		case '0':
			newRoomRouter(socket, data)
			break
		default:
			roomLobby(socket, data)
			break
	}
}

module.exports = menusRouter