const server = require('net').createServer()
const welcome = require('./templates/welcome')
const { setSocket, getSockets } = require('./classes/Sockets')

const menusRouter = require('./menus/menusRouter')

server.on('connection', socket => {
	socket.setEncoding('utf8')
  setSocket(socket)
	
  console.log(`Client ${socket.id} connected`)
  socket.write(`${welcome}\n`)
  socket.write('Enter your name: \n')

  socket.on('data', data => {
		data = data.trim()

		// First message
		if (!socket.name) {
			if (!data) data = `Guest ${socket.id}`
			// ************************* CHECK IF THE NAME ALREADY EXISTS
			socket.name = data
			socket.write(`\n* Welcome ${socket.name}! *\n`)
			data = null
		}
		menusRouter(socket, data)
	})
	
	socket.on('close', () => {
		delete sockets[socket.id]
		console.log(`Client ${socket.id} disconnected`)
	})
	socket.on('error', (err) => {
		console.log('ERROR: ', err)
	})
})

server.listen(8000, () => console.log('Server listening'))