CONFIG
Name of the votation: Dinner
Description: We'll discuse what to do for dinner
Minimum number of participants: 3
Maximum number of participants: 5
Quorum (send ? for more info): 3
TOPICS
1. Shall we order food instead of cooking?
a. Yes	b. No
2. What should we eat for dinner?
a. Pasta	b. Pizza	c. Sushi	
3. Who will wash the dishes?
a. Jhon	b. Anne	c. Margaret	d. Peter	
