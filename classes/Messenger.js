let { getSockets } = require('./Sockets')
const { isInRoom } = require('./Rooms')

class Messenger {
	sendEverybody(text, room, participant) {
		const sockets = getSockets()
		// If room is passed, just send to participants in that room
		// If socket is passed, exclude it from the message
		for (let socket in sockets) {
			const actualSocket = sockets[socket]
			if (room) {
				if (!isInRoom(room, actualSocket.name)) return
			}
			if (socket == participant) return
			actualSocket.write(text)
		}
	}
}

module.exports = new Messenger()