let sockets = {}
let idCounter = 1

class Sockets {
	getSockets() {
		return sockets
	}

	setSocket(socket) {
		socket.id = idCounter ++
		sockets[socket.id] = socket
	}
}

module.exports = new Sockets()