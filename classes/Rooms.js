const fs = require('fs')
const { promisify } = require('util')
const readFile = promisify(fs.readFile)

const folder = './topics/'

let rooms = {}
let roomIdCounter = 1

const initializeRooms = Symbol('initializeRooms')
const getRoomContent = Symbol('getRoomContent')
const getRoomConfig = Symbol('getRoomConfig')

class Rooms {
	constructor() {
		this.initializeRooms()
	}

	addRoom(room) {
		rooms[room] = {}
	}

	removeRoom(room) {
		delete rooms[room]
	}

	joinParticipant(room, participant) {
		rooms[room].participants.push(participant)
		rooms[room].current ++
	}

	removeParticipant(room, participant) {
		const participantIndex = rooms[room].participants.findIndex(person => person == participant)
		rooms[room].participants.splice(participantIndex, 1)
		rooms[room].current --
	}

	isInRoom(room, participant) {
		return (rooms[room].participants.includes(participant))
	}

	openRoom(room) {
		rooms[room].open = true
	}

	closeRoom(room) {
		rooms[room].open = false
	}

	getRooms(room) {
		if (!room) return rooms

		return rooms[room]
	}

	getReady(room) {
		return rooms[room].ready
	}

	setReady(room, ready) {
		const actualRoom = rooms[room]
		if (ready) actualRoom.ready ++
		if (!ready) actualRoom.ready --

		if (actualRoom.ready == actualRoom.current && actualRoom.current >= actualRoom.min ) {
			return 'ready'
		} 
	}

	initializeRooms() {
		return new Promise((resolve, reject) =>{
			try {
				fs.readdir(folder, async (err, files) => {
					for (let file of files) {
						const path = folder + file
				
						const f = await readFile(path)
						const stringConfig = f.toString().substring(0, f.indexOf('TOPICS'))
						const { roomId, room } = this.getRoomConfig(stringConfig)
						rooms[roomId] = room
					}
					resolve()
				})
			} catch (error) {
				reject(error)
			}
		})
	}

	async getRoomContent(room) {
		const path = folder + rooms[room].name + '.txt'
		const file = await readFile(path)
		const stringContent = file.toString().substring(stringContent.indexOf('TOPICS'))
		const arrayLines = stringContent.toString().split('\r\n')
		arrayLines.shift()

		return arrayLines
	}

	getRoomConfig(stringConfig) {
	
		const arrayLines = stringConfig.split('\r\n')
				
		let name = arrayLines.find((line => line.includes('Name')))
		name = name.split(': ')[1]
		let desc = arrayLines.find((line => line.includes('Desc')))
		desc = desc.split(': ')[1]
		let min = arrayLines.find((line => line.includes('Min')))
		min = parseInt(min.split(': ')[1])
		let max = arrayLines.find((line => line.includes('Max')))
		max = parseInt(max.split(': ')[1])
		let quorum = arrayLines.find((line => line.includes('Quorum')))
		quorum = parseInt(quorum.split(': ')[1])
	
	
		const room = {
			name,
			desc,
			min,
			max,
			quorum,
			current: 0,
			open: true,
			participants: [],
			ready: 0
		}		
		return { roomId: roomIdCounter ++, room }
	}

}

module.exports = new Rooms()